-- creating the Manager table separately
CREATE TABLE Manager (
    Manager_id INT PRIMARY KEY,
    Manager_name VARCHAR(255),
    Manager_location VARCHAR(255)
);

-- creating the Contract table separately
CREATE TABLE Contract (
    Contract_id INT PRIMARY KEY,
    Estimated_cost INT,
    Completion_date DATE
);


-- creating the Staff table separately
CREATE TABLE Staff (
    Staff_id INT PRIMARY KEY,
    Staff_name VARCHAR(255),
    Staff_location VARCHAR(255)
);

--creating the Client table along with Manager_id as foreign key
CREATE TABLE Client (
    Client_id INT PRIMARY KEY,
    Name VARCHAR(255),
    Location VARCHAR(255),
    Manager_id INT,
    FOREIGN KEY (Manager_id) REFERENCES Manager(Manager_id)
);

--creating the intermediate table for Client and Contract tables as they have many-to-many relationship
CREATE TABLE ClientContract (
    Client_id INT,
    Contract_id INT,
    PRIMARY KEY (Client_id, Contract_id),
    FOREIGN KEY (Client_id) REFERENCES Client(Client_id),
    FOREIGN KEY (Contract_id) REFERENCES Contract(Contract_id)
);



