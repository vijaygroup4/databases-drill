-- creating the Patient table
CREATE TABLE Patient (
    Patient_id INT PRIMARY KEY,
    Name VARCHAR(255),
    DOB DATE,
    Address VARCHAR(255)
);

-- creating the Prescription table
CREATE TABLE Prescription (
    Prescription_id INT PRIMARY KEY,
    Drug VARCHAR(255),
    Date DATE,
    Dosage VARCHAR(50),
    Doctor VARCHAR(255),
    Secretary VARCHAR(255)
);

-- creating the intermediate table for Patient and Prescription as they have many-to-many relationships
CREATE TABLE PatientPrescription (
    Patient_id INT,
    Prescription_id INT,
    PRIMARY KEY (Patient_id, Prescription_id),
    FOREIGN KEY (Patient_id) REFERENCES Patient(Patient_id),
    FOREIGN KEY (Prescription_id) REFERENCES Prescription(Prescription_id)
);
