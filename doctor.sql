-- creating the Doctor table
CREATE TABLE Doctor (
    Doctor_id INT PRIMARY KEY,
    DoctorName VARCHAR(255),
    Secretary VARCHAR(255)
);

-- creating the Patient table
CREATE TABLE Patient (
    Patient_id INT PRIMARY KEY,
    PatientName VARCHAR(255),
    PatientDOB DATE,
    PatientAddress VARCHAR(255)
);

-- creating the intermediate table for Doctor and Patient
CREATE TABLE Prescription (
    Prescription_id INT PRIMARY KEY,
    Drug VARCHAR(255),
    Date DATE,
    Dosage VARCHAR(50),
    Doctor_id INT,
    Patient_id INT,
    FOREIGN KEY (Doctor_id) REFERENCES Doctor(Doctor_id),
    FOREIGN KEY (Patient_id) REFERENCES Patient(Patient_id)
);
