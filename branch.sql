--creating the Branch table
CREATE TABLE Branch(
    Branch_id INT PRIMARY KEY,
    Branch_Addr VARCHAR(255)
);

-- creating the Book table
CREATE TABLE Book(
    ISBN VARCHAR(255) PRIMARY KEY,
    Title VARCHAR(255),
    Author VARCHAR(255),
    Publisher VARCHAR(255)
);

-- creating the intermediate table as Book and Branch have many-to-many relationships
CREATE TABLE BranchBook(
    Branch_id INT,
    ISBN VARCHAR(255),
    Num_copies INT,
    PRIMARY KEY(Branch_id,ISBN),
    FOREIGN KEY(Branch_id) REFERENCES Branch(Branch_id),
    FOREIGN KEY(ISBN) REFERENCES Book(ISBN)
);